# PHP Composer docker image

This image was created to include php, git, Heroku CLI, and composer.

* PHP 7.2
* Composer 1.7
* Heroku cli

[3.1 Dockerfile](https://bitbucket.org/dauntless-agency/php-composer/src/3.1/Dockerfile?at=3.1&fileviewer=file-view-default)

[1.20 Dockerfile](https://bitbucket.org/dauntless-agency/php-composer/src/1.20/Dockerfile)

## Tags
`3.*` is for PHP 7.2

`1.*` is for PHP 7.0
